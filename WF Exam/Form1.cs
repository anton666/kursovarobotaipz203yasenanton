﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace WF_Exam
{
    public partial class Questionform : Form
    {
        enum Nums : int { Minone = -1, Null, One, Two, Three, Four, Five,  Onehundred = 100 };
        string filtr = "TextFiles(*.txt;*.log)|*.txt;*log|All files(*.*)|*.*";
        string PathMath = @"Math\\Math.txt";
        string PathOOP = @"OOP\\OOP.txt";
        string code = "1234";

        public Questionform()
        {
            InitializeComponent();

        }
       
        private void btAddQ_Click(object sender, EventArgs e)
        {
            try
            {
                listView1.Items.Add(new ListViewItem(new string[] { "", "", "", "", "", "" }));
                foreach (ListViewItem i in this.listView1.Items)
                {
                    General.GetQ = () => i.SubItems[(int)Nums.Null].Text;
                    General.GetA = () => i.SubItems[(int)Nums.One].Text;
                    General.GetB = () => i.SubItems[(int)Nums.Two].Text;
                    General.GetC = () => i.SubItems[(int)Nums.Three].Text;
                    General.GetD = () => i.SubItems[(int)Nums.Four].Text;
                    General.GetCor = () => i.SubItems[(int)Nums.Five].Text;

                    
                    General.SetQ = (x) => i.SubItems[(int)Nums.Null].Text = x;
                    General.SetA = (x) => i.SubItems[(int)Nums.One].Text = x;
                    General.SetB = (x) => i.SubItems[(int)Nums.Two].Text = x;
                    General.SetC = (x) => i.SubItems[(int)Nums.Three].Text = x;
                    General.SetD = (x) => i.SubItems[(int)Nums.Four].Text = x;
                    General.SetCor = (x) => i.SubItems[(int)Nums.Five].Text = x;

                }
                
                var add = new ADD_data();
                add.ShowDialog();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
       
        private void addQuestionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btAddQ_Click(sender, e);
        }
        
        private void addQuestionToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            btAddQ_Click(sender, e);
        }
        
        private void btSave_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder test = new StringBuilder();
                foreach (ListViewItem item in listView1.Items)
                {
                    foreach (ListViewItem.ListViewSubItem subItem in item.SubItems)
                    {
                        test.Append(subItem.Text);
                        test.Append(';');
                    }
                    test.Append(Environment.NewLine);
                }

                if (rbNet.Checked)
                {
                    File.WriteAllText(PathMath, test.ToString());
                    MessageBox.Show("Файл було збережено", "Information",
                              MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                else if (rbPlus.Checked)
                {
                    File.WriteAllText(PathOOP, test.ToString());
                    MessageBox.Show("Файл було збережено", "Information",
                             MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (!rbNet.Checked || !rbPlus.Checked) 
                {
                    MessageBox.Show("Будь-ласка оберіть предмет!", "Ошибка!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
      
        private void saveFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btSave_Click(sender, e);
        }
       
        private void saveFileToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            btSave_Click(sender, e);
        }
        
        private void Questionform_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                var q = MessageBox.Show("Ви впевнені що хочете закрити?", "WARNING!", MessageBoxButtons.YesNo);
                if (q == DialogResult.Yes)
                {
                    e.Cancel = false;
                }

                else
                    e.Cancel = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        private void btLoadTest_Click(object sender, EventArgs e)
        {
            StreamReader s = null;
            string allLines = null;
            string Path = null;
            try
            {
                if (rbNet.Checked)
                {
                    Path = PathMath;
                }
                else if (rbPlus.Checked)
                {
                    Path = PathOOP;
                }

                else if (!rbNet.Checked && !rbPlus.Checked)
                {
                    MessageBox.Show("Будь-ласка оберіть тест!", "Ошибка!",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                using (s = new StreamReader(Path, true))
                    do
                    {
                        allLines = s.ReadLine();
                        listView1.Items.Add(new ListViewItem
                            (allLines.Split(new char[] { ';', '\n' })));
                    }
                    while (!s.EndOfStream);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (s != null)
                {
                    s.Close();
                    s = null;
                }
            }
        }
        
        private void loadFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.btLoadTest_Click(sender, e);
        }
       
        private void loadTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btLoadTest_Click(sender, e);
        }
       
        private void btCorrect_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.listView1.SelectedIndices == null)
                    return;
                else
                {
                    foreach (ListViewItem i in this.listView1.SelectedItems)
                    {

                        General.GetQ = () => i.SubItems[(int)Nums.Null].Text;
                        General.GetA = () => i.SubItems[(int)Nums.One].Text;
                        General.GetB = () => i.SubItems[(int)Nums.Two].Text;
                        General.GetC = () => i.SubItems[(int)Nums.Three].Text;
                        General.GetD = () => i.SubItems[(int)Nums.Four].Text;
                        General.GetCor = () => i.SubItems[(int)Nums.Five].Text;

                        ///delegates to setting data to ADDdataform
                        General.SetQ = (x) => i.SubItems[(int)Nums.Null].Text = x;
                        General.SetA = (x) => i.SubItems[(int)Nums.One].Text = x;
                        General.SetB = (x) => i.SubItems[(int)Nums.Two].Text = x;
                        General.SetC = (x) => i.SubItems[(int)Nums.Three].Text = x;
                        General.SetD = (x) => i.SubItems[(int)Nums.Four].Text = x;
                        General.SetCor = (x) => i.SubItems[(int)Nums.Five].Text = x;
                        var add = new ADD_data();
                        add.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        private void correctFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btCorrect_Click(sender, e);
        }
       
        private void correctFileToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            btCorrect_Click(sender, e);
        }
        
        private void btDeleteTest_Click(object sender, EventArgs e)
        {
            try
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    var f = filtr;
                    if (File.Exists(this.openFileDialog1.FileName))
                    {
                        var q = MessageBox.Show(string.Format("Ви впевнені, що хочете видалити тест {0} ?", this.openFileDialog1.FileName),
                            "WARNING!", MessageBoxButtons.YesNoCancel);
                        if (q == DialogResult.Yes)
                        {
                            File.Delete(this.openFileDialog1.FileName);
                            MessageBox.Show("Файл був видалений!", "Information",
                                   MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        private void deleteFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.btDeleteTest_Click(sender, e);
        }
        
        private void deleteFileToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.btDeleteTest_Click(sender, e);
        }
       
        private void rbStud_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                   rbTeacher.Enabled = false;
                    listView1.Visible = false;
                    btAddQ.Visible = false;
                    btCorrect.Visible = false;
                    btDeleteTest.Visible = false;
                    btLoadTest.Visible = false;
                    btSave.Visible = false;
                    btClear.Visible = false;
                    btWatch.Visible = false;
                    btSavetest.Visible = true;
                    tbCode.Visible = false;
                    btRes.Visible = false;
                    lbLn.Visible = true;
                    lbN.Visible = true;
                    lbF.Visible = true;
                    lbg.Visible = true;
                    tbLname.Visible = true;
                    tbName.Visible = true;
                    tbFname.Visible = true;
                    tbGroup.Visible = true;
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
       
        private void btStart_Click(object sender, EventArgs e)
        {
            int count = (int)Nums.Null;
            string coransw = null;
            int questions = (int)Nums.Null;
            try
            {
                if (this.tbName.Text == string.Empty || this.tbFname.Text == string.Empty || this.tbLname.Text == string.Empty)
                    MessageBox.Show("Необхідно ввести ПІБ группу та обрати тест!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                else
                {
                    if (rbNet.Checked || rbPlus.Checked)
                    {
                        this.listView1.Items.Clear();
                        this.btLoadTest_Click(sender, e);
                       

                        foreach (ListViewItem i in this.listView1.Items)
                        {
                            General.GetQ = () => i.SubItems[(int)Nums.Null].Text;
                            General.GetA = () => i.SubItems[(int)Nums.One].Text;
                            General.GetB = () => i.SubItems[(int)Nums.Two].Text;
                            General.GetC = () => i.SubItems[(int)Nums.Three].Text;
                            General.GetD = () => i.SubItems[(int)Nums.Four].Text;
                            General.GetCor = () => i.SubItems[(int)Nums.Five].Text;

                           
                            General.SetQ = (x) => i.SubItems[(int)Nums.Null].Text = x;
                            General.SetA = (x) => i.SubItems[(int)Nums.One].Text = x;
                            General.SetB = (x) => i.SubItems[(int)Nums.Two].Text = x;
                            General.SetC = (x) => i.SubItems[(int)Nums.Three].Text = x;
                            General.SetD = (x) => i.SubItems[(int)Nums.Four].Text = x;
                            General.SetCor = (x) => i.SubItems[(int)Nums.Five].Text = x;

                            coransw = i.SubItems[(int)Nums.Five].Text;

                           
                            var add = new ADD_data();
                            add.tbCorrect.Visible = false;
                            add.lbcoransw.Visible = false;
                            add.ShowDialog();
                            tbCoransw.Text = count.ToString();

                            if (add.rbA.Checked)
                            {
                                if (coransw == "1")
                                {
                                    count++;
                                }
                            }
                            if (add.rbB.Checked)
                            {
                                if (coransw == "2")
                                {
                                    count++;
                                }
                            }
                            if (add.rbC.Checked)
                            {
                                if (coransw == "3")
                                {
                                    count++;
                                }
                            }
                            if (add.rbD.Checked)
                            {
                                if (coransw == "4")
                                {
                                    count++;
                                }
                            }

                            tbCoransw.Text = count.ToString();
                            questions++;
                        }
                        {
                            var res = count * (int)Nums.Onehundred / questions;
                            tbRes.Text =  res.ToString();
                            
                         
                        }
                    }
                    if (!rbNet.Checked && !rbPlus.Checked)
                    {
                        MessageBox.Show("Будь-ласка оберіть предмет!", "Ошибка!",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        private void rbTeacher_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                var t = new Teacher();
                t.ShowDialog();

                rbStud.Enabled = false;
                listView1.Visible = true;
                btAddQ.Visible = true;
                btCorrect.Visible = true;
                btDeleteTest.Visible = true;
                btLoadTest.Visible = true;
                btSave.Visible = true;
                btClear.Visible = true;
                btWatch.Visible = true;
                tbCode.Visible = true;
                btRes.Visible = true;
                btSave.Visible = true;
                lbLn.Visible = true;
                lbN.Visible = true;
                lbF.Visible = true;
                lbg.Visible = true;
                tbLname.Visible = true;
                tbName.Visible = true;
                tbFname.Visible = true;
                tbGroup.Visible = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        
        private void btClear_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            tbCode.Clear();
        }
        
        private void clearDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.btClear_Click(sender, e);
        }
       
        private void clearDataToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.btClear_Click(sender, e);
        }
        
        private void btSavetest_Click(object sender, EventArgs e)
        {
            string l = tbLname.Text;
            string n = tbName.Text;
            string f = tbFname.Text;
            string g = tbGroup.Text;
            string a = tbCoransw.Text;
            string r = tbRes.Text;

            try
            {
                tbCode.Text =string.Format( "{0} {1} {2} група{3} правильних {4} оцінка за тест {5} \r\n" ,l,n,f,g,a,r);
                File.AppendAllText("result.txt", tbCode.Text);
                tbCode.Text = File.ReadAllText("result.txt");
                var buf = Encoding.Unicode.GetBytes(tbCode.Text);
                var key = Encoding.Unicode.GetBytes(code);
                for (var i = 0; i < buf.Length; ++i)
                    buf[i] ^= key[i % key.Length];

                tbCode.Text = Encoding.Unicode.GetString(buf);
                File.WriteAllText("code.txt", tbCode.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
       
        private void btRes_Click(object sender, EventArgs e)
        {
            try
            {
                if (File.Exists("code.txt"))
                {
                    tbCode.Text = File.ReadAllText("code.txt");
                    var buf = Encoding.Unicode.GetBytes(this.tbCode.Text);
                    var key = Encoding.Unicode.GetBytes(code);
                    for (var i = 0; i < buf.Length; ++i)
                        buf[i] ^= key[i % key.Length];
                    tbCode.Text = Encoding.Unicode.GetString(buf);
                }
                else
                    MessageBox.Show("Файл було не знайдено", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
       
        private void showResultsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.btRes_Click(sender, e);
        }
        
        private void showResultsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.btRes_Click(sender, e);
        }
        
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ab = new AboutBox1();
            ab.ShowDialog();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
