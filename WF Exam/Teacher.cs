﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace WF_Exam
{
    public partial class Teacher : Form
    { 
      
        string log = "admin";
        string passw = "admin";

        public Teacher()
        {
            InitializeComponent();
        }
       
        private void btOk_Click(object sender, EventArgs e)
        {                       
                if (this.tbLogin.Text == string.Empty || this.tbPassw.Text == string.Empty)
                {
                    MessageBox.Show("Введите логин и пароль", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                else if (this.tbLogin.Text != string.Empty && this.tbPassw.Text != string.Empty && log == this.tbLogin.Text && passw == this.tbPassw.Text)
                {
                  
                Close();

                }
                else if (this.tbLogin.Text != string.Empty && this.tbPassw.Text != string.Empty && log != this.tbLogin.Text || passw != this.tbPassw.Text)
                    MessageBox.Show("Неправильний логін або пароль", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
           
        }
    }
}
